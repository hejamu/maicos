=================
Structure modules
=================

Saxs
====

Description
***********

.. automodule:: maicos.modules.structure.saxs
    :members:
    :undoc-members:
    :show-inheritance:

Diporder
========

Description
***********

.. automodule:: maicos.modules.structure.diporder
    :members:
    :undoc-members:
    :show-inheritance:

Debyer
======

Description
***********

.. automodule:: maicos.modules.structure.debye
    :members:
    :undoc-members:
    :show-inheritance:
