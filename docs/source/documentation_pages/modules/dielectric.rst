===========================
Dielectric constant modules
===========================

Epsilon bulk
============

Description
***********

.. automodule:: maicos.modules.epsilon.epsilon_bulk
    :members:
    :undoc-members:
    :show-inheritance:


Epsilon planar
==============

Description
***********

.. automodule:: maicos.modules.epsilon.epsilon_planar
    :members:
    :undoc-members:
    :show-inheritance:

Epsilon cylinder
================

Description
***********

.. automodule:: maicos.modules.epsilon.epsilon_cylinder
    :members:
    :undoc-members:
    :show-inheritance:

Dielectric spectrum
===================

Description
***********


.. automodule:: maicos.modules.epsilon.dielectric_spectrum
    :members:
    :undoc-members:
    :show-inheritance:
