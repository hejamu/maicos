==================
Timeseries modules
==================

Dipole angle
============

Description
***********

.. automodule:: maicos.modules.timeseries.dipole_angle
    :members:
    :undoc-members:
    :show-inheritance:

Kinetic energy
==============

Description
***********

.. automodule:: maicos.modules.timeseries.kinetic_energy
    :members:
    :undoc-members:
    :show-inheritance:
