
Maintainer
----------

- Philip Loche

Authors
-------

- Alexander Schlaich
- Philip Loche

Contributors
------------

- Maximilian Becker
- Shane Carlson
- Julian Kappler
- Julius Schulz
- Dominik Wille
- Amanuel Wolde-Kidan
- Philipp Stärk
- Simon Gravelle
- Henrik Jaeger
